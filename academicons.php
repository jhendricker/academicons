<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sciences.ucf.edu/it
 * @since             1.0.0
 * @package           Academicons
 *
 * @wordpress-plugin
 * Plugin Name:       Academicons
 * Description:       Allows for use of Academicons using the Better Font Awesome plugin shorcode base. Better Font Awesome must be activated prior to plugin activation. E.g. [icon unprefixed_class="ai ai-acclaim-square ai-3x"]
 * Version:           1.8.2
 * Author:            Jonathan Hendricker
 * Author URI:        https://sciences.ucf.edu/it
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       academicons
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.8.2' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-academicons-activator.php
 */
function activate_academicons() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-academicons-activator.php';
	Academicons_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-academicons-deactivator.php
 */
function deactivate_academicons() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-academicons-deactivator.php';
	Academicons_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_academicons' );
register_deactivation_hook( __FILE__, 'deactivate_academicons' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-academicons.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_academicons() {

	$plugin = new Academicons();
	$plugin->run();

}
run_academicons();
