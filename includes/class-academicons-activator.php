<?php

/**
 * Fired during plugin activation
 *
 * @link       https://sciences.ucf.edu/it
 * @since      1.0.0
 *
 * @package    Academicons
 * @subpackage Academicons/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Academicons
 * @subpackage Academicons/includes
 * @author     College of Sciences <cosweb@ucf.edu>
 */
class Academicons_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// Require parent plugin
	    if ( ! is_plugin_active( 'better-font-awesome/better-font-awesome.php' ) and current_user_can( 'activate_plugins' ) ) {
	        // Stop activation redirect and show error
	        wp_die('Sorry, but this plugin requires the Better Font Awesome Plugin to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
	    }

	}

}
