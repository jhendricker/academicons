<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://sciences.ucf.edu/it
 * @since      1.0.0
 *
 * @package    Academicons
 * @subpackage Academicons/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Academicons
 * @subpackage Academicons/includes
 * @author     College of Sciences <cosweb@ucf.edu>
 */
class Academicons_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
